import json
import socket
import sys
import math

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.gameTick = -1
        self.afterCrash = -1
        self.cornerReduce = 0.3
        self.cornerGoodAngle = 35

    def msg(self, msg_type, data):
        if self.gameTick == -1:
            self.send(json.dumps({"msgType": msg_type, "data": data}))
        else:
            self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.gameTick}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name, "key": self.key})
                                 
    # For test only
    def creatRace(self):
        data = '{"msgType": "createRace", "data": {"botId": {"name": "VietHiP", "key": "T6joAKNekqPSNA"},"trackName": "germany", "password": "vvm", "carCount": 1}}'
        self.send(data)
        
    def throttle(self, throttle):
        self.lastThrottle = throttle
        self.msg("throttle", throttle)
        
    def switchlane(self, lane):
        print("SWITCH LANE:")
        print lane
        self.msg("switchLane", lane)
    
    def get_angle(self, data):
        for item in data:
            if item['id']['name'] == self.name:
                return int(item['angle'])
        return 0
        
    def get_piecePosition(self, data):
        for item in data:
            if item['id']['name'] == self.name:
                return item['piecePosition']
        return 'null'
    
    def isCarInFront(self, data):
        myLane = -1000
        myDistance = 0
        frontLane = -1000
        currPiece = self.get_piece(self.currIdxPiece)
        #Get my info
        if not ('switch' in currPiece):
            for item in data:
                if item['id']['name'] == self.name:
                        myLane = item['piecePosition']['lane']['startLaneIndex']
                        myDistance = item['piecePosition']['inPieceDistance']
                        break;
            #Get car in front
            for item in data:
                if (item['id']['name'] != self.name) and (myLane == item['piecePosition']['lane']['startLaneIndex']):
                    if myDistance < item['piecePosition']['inPieceDistance']:
                        print("Has car in front you can kick it ;)")
                        return True 
        return False
        
    def getDistanceCarInFront(self, data):
        myLane = -1000
        myDistance = 0
        frontLane = -1000
        currPiece = self.get_piece(self.currIdxPiece)
        #Get my info
        #if not ('switch' in currPiece):
        for item in data:
                if item['id']['name'] == self.name:
                        myLane = item['piecePosition']['lane']['startLaneIndex']
                        myDistance = item['piecePosition']['inPieceDistance']
                        break;
        #Get car in front
        for item in data:
                if (item['id']['name'] != self.name) and (myLane == item['piecePosition']['lane']['startLaneIndex']):
                    if myDistance < item['piecePosition']['inPieceDistance']:
                        return item['piecePosition']['inPieceDistance'] - myDistance
        return -1
    
    def get_currentIdxPiece(self, data):
        for item in data:
            if item['id']['name'] == self.name:
                return item['piecePosition']['pieceIndex']
        print("get_currentPiece NOT FOUND!")
        print data
        return -1
        
    def get_laneInPiece(self):
        for item in self.carPositions:
            if item['id']['name'] == self.name:
                return item['piecePosition']['lane']['endLaneIndex']
        print("get_laneInPiece NOT FOUND!")
        return -1000
        
    def get_piece(self, index):
        if index > self.get_countPiece():
            index = index - self.get_countPiece()
        elif index < 0:
            index = self.get_countPiece() + index
        i = 0
        for item in self.pieces:
            if i == index:
                return item
            i = i + 1
        print("get_piece NOT FOUND:")
        print index
        return 'null'
            
    def get_nearPiece(self, currIdx, far):
        index = currIdx + far
        count = self.get_countPiece()
        if index >= count:
            index = index - count
        else:
            if index < 0:
                index = count - abs(index)        
                i = 0
        oReturn = self.get_piece(index)
        if oReturn is 'null':
            print("get_nearPiece fail")
            print currIdx
            print far
            print index
            
        return oReturn
        
    def get_countPiece(self):
        return len(self.pieces)
        
    def get_lane(self, index):
        i = 0
        for item in self.lanes:
            if i == index:
                return item
            i = i + 1
        print("get_lane NOT FOUND!")
        return 'null'
        
    def get_speed(self, currPiece):
        if (self.lastPiece is 'null') or (currPiece is 'null'):
            print(">>>Speed(null): -1")
            self.currSpeed = -1;
            self.lastPiece = currPiece
            self.currPieceDistance = -1
        else:
            currPieceDistance = 0
            lastPieceDistance = 0
            distance = 0
            if 'inPieceDistance' in currPiece:
                currPieceDistance = currPiece['inPieceDistance']
            if 'inPieceDistance' in self.lastPiece:
                lastPieceDistance = self.lastPiece['inPieceDistance']
            if currPiece['pieceIndex'] == self.lastPiece['pieceIndex']:
                distance = currPieceDistance - lastPieceDistance
            else:
                piece = 'null'
                if not (self.lastPiece is 'null'):
                    piece = self.get_piece(self.lastPiece['pieceIndex'])
                if (not (piece is 'null')):
                    if ('length' in piece):
                        distance = piece['length'] - lastPieceDistance
                    else:
                        distance = (3.14 * piece['radius'] *  piece['angle'])/float(180) - lastPieceDistance
                elif self.currSpeed > 0:
                    distance = distance + self.currSpeed
                else:
                    distance = distance + 3
                distance = currPieceDistance + distance
                
            ticks = self.gameTick - self.lastGameTick
            if (ticks > 0) and (distance > 0):
                self.lastSpeed = self.currSpeed
                self.currSpeed = distance / ticks
                # Maybe incorrect speed we need change.
                if (self.currSpeed > (self.lastSpeed + 6.0)) and (not  self.inTurboMode) and (self.currSpeed > -1)  and (self.lastSpeed > -1):
                    if self.lastThrottle >= 0.9:
                        self.currSpeed = self.lastSpeed + 0.1
                    elif self.lastThrottle >= 0.7:
                        self.currSpeed = self.lastSpeed + 0.05
                    elif self.lastThrottle > 0.5:
                        self.currSpeed = self.lastSpeed + 0.01
                    elif self.lastThrottle == 0.5:
                        self.currSpeed = self.lastSpeed
                    elif self.lastThrottle >= 0.4:
                        self.currSpeed = self.lastSpeed - 0.01
                    elif self.lastThrottle >= 0.3:
                        self.currSpeed = self.lastSpeed - 0.05
                    else :
                        self.currSpeed = self.lastSpeed - 0.1
                    print("***Correct speed:")
                    print self.currSpeed
            else:
                print(">>>Speed: -1")
                self.currSpeed = -1
                self.lastSpeed = -1
                print self.lastPiece
                print currPiece
            self.lastPiece = currPiece
            
    def set_incAI(self, currIdxPiece):
        idx = currIdxPiece
        lastRadius = -1
        lastAngle = 0
        
        while True:
            if idx < 0:
                idx = self.get_countPiece() - 1
            nextPiece = self.get_piece(idx)
            if not ('radius' in nextPiece):
                break
            if lastRadius == -1:
                lastRadius = nextPiece['radius']
                lastAngle = nextPiece['angle']
            elif (lastRadius != nextPiece['radius']):
                break
            curAngle = nextPiece['angle']
            if lastAngle > 0:
                if (lastAngle - curAngle) > (lastAngle + curAngle):
                    break;
            elif lastAngle < 0:
                if (lastAngle - curAngle) < (lastAngle + curAngle):
                    break;
            idx = idx - 1
            inc = 0
            if ('inceaseAI' in nextPiece):
                    inc = nextPiece['inceaseAI']            
            if self.incAI > 0:
                inc = inc + self.incAI
            else:
                inc = 0
            if inc > 0.15:
                inc = 0.15
            nextPiece['inceaseAI'] = inc 
            print("[][][]Set inc AI for piece:")
            print idx
            
        idx = currIdxPiece + 1
        while True:
            if idx >= self.get_countPiece():
                idx = 0
            nextPiece = self.get_piece(idx)
            if not ('radius' in nextPiece):
                break
            if lastRadius == -1:
                lastRadius = nextPiece['radius']
                lastAngle = nextPiece['angle']
            elif (lastRadius != nextPiece['radius']):
                break
            curAngle = nextPiece['angle']
            if lastAngle > 0:
                if (lastAngle - curAngle) > (lastAngle + curAngle):
                    break;
            elif lastAngle < 0:
                if (lastAngle - curAngle) < (lastAngle + curAngle):
                    break;
            idx = idx + 1
            inc = 0
            if ('inceaseAI' in nextPiece):
                inc = nextPiece['inceaseAI']
            if self.incAI > 0:
                inc = inc + self.incAI
            else:
                inc = 0
            if inc > 0.1:
                inc = 0.1
            nextPiece['inceaseAI'] = inc
            print("[][][]Set inc AI for piece:")
            print idx
    
    def set_reduceAI(self, currIdxPiece):
        idx = currIdxPiece
        lastRadius = -1
        lastAngle = 0
        while True:
            if idx < 0:
                idx = self.get_countPiece() - 1
            nextPiece = self.get_piece(idx)
            if not ('radius' in nextPiece):
                break
            if lastRadius == -1:
                lastRadius = nextPiece['radius']
                lastAngle = nextPiece['angle']
            elif (lastRadius != nextPiece['radius']):
                break
            curAngle = nextPiece['angle']
            if lastAngle > 0:
                if (lastAngle - curAngle) > (lastAngle + curAngle):
                    break;
            elif lastAngle < 0:
                if (lastAngle - curAngle) < (lastAngle + curAngle):
                    break;
            idx = idx - 1
            reduce = 0
            if ('reduceAI' in nextPiece):
                    reduce = nextPiece['reduceAI']
            nextPiece['reduceAI'] = reduce + 0.1
            print("[][][]Set reduce AI base on crash:")
            print idx
            
        idx = currIdxPiece + 1
        while True:
            if idx >= self.get_countPiece():
                idx = 0
            nextPiece = self.get_piece(idx)
            if not ('radius' in nextPiece):
                break
            if lastRadius == -1:
                lastRadius = nextPiece['radius']
            elif (lastRadius != nextPiece['radius']):
                break
            curAngle = nextPiece['angle']
            if lastAngle > 0:
                if (lastAngle - curAngle) > (lastAngle + curAngle):
                    break;
            elif lastAngle < 0:
                if (lastAngle - curAngle) < (lastAngle + curAngle):
                    break;
            idx = idx + 1
            reduce = 0
            if ('reduceAI' in nextPiece):
                    reduce = nextPiece['reduceAI']
            nextPiece['reduceAI'] = reduce + 0.1
            print("[][][]Set reduce AI base on crash:")
            print idx
    
    def get_throttle(self, currIdxPiece):
        # After crash we need speedup.
        if (self.afterCrash == self.currIdxPiece):
            return 1
        else:
            self.afterCrash = -1
        currPiece = self.get_piece(currIdxPiece)
        currAngle = 0
        inPieceDistance = 0
        lengthGood = -1
        if currPiece is not 'null':
            if 'angle' in currPiece:
                currAngle = abs(currPiece['angle'])
            if 'inPieceDistance' in currPiece:
                inPieceDistance = currPiece['inPieceDistance']
                if ('angle' in currPiece):
                    inPieceDistance = (3.14 * currPiece['radius'] *  currPiece['angle'])/float(180) - inPieceDistance
                else:
                    inPieceDistance = currPiece['length'] - inPieceDistance
                
        lastAngle = abs(self.get_AngleInFront(currIdxPiece, -1))
        nextAngle = abs(self.get_AngleInFront(currIdxPiece, 1))
        iReturn = 1
        flatInFront = self.get_countFlatInFront(self.currIdxPiece)
        # We don't known speed. Use default.
        if self.currSpeed <= 0:
            print("Unknown speed using throttle base on car Angle")
            iReturn = 1 - float(self.currAngle) / float(self.cornerGoodAngle)
            if (iReturn > 1):
                iReturn = 1
            elif iReturn < 0:
                iReturn = 0
            return iReturn
        
        
        # Brake before go to corner
        if self.inTurboMode and (currAngle == 0) and (flatInFront > 1):
            lengthGood = self.get_lengthFlatInFront(currIdxPiece, self.carPositions) 
            if inPieceDistance > 0:
                    lengthGood = lengthGood + inPieceDistance
            print(">>>Brake in TURBO MODE flatInFront:")
            print lengthGood
            if (lengthGood <= 200):
                return 0.3
            elif (lengthGood <= 400):
                return 0.5
            elif (lengthGood <= 600):
                return 0.6
            elif (lengthGood <= 700):
                return 0.8
                
        if (currAngle == 0):
            self.incAI = 0
            iReturn = 1
            speedOK = self.calcCornerSpeed (currIdxPiece + flatInFront + 1)
            if (speedOK > 0):
                cornerAngle = abs(self.get_AngleInCorner(currIdxPiece + flatInFront + 1))
                if (cornerAngle <= 45):
                    speedOK = speedOK + speedOK*(1 - float(cornerAngle)/float(45)) 
                lengthGood = self.get_lengthFlatInFront(currIdxPiece, self.carPositions) 
                piece = self.get_piece(currIdxPiece)
                if inPieceDistance > 0:
                    lengthGood = lengthGood + inPieceDistance
                lengthSpeedDown = (self.currSpeed - speedOK) * 47
                if (not  self.inTurboMode):
                    lengthSpeedDown = lengthSpeedDown
                else:
                    lengthSpeedDown =  lengthSpeedDown + lengthSpeedDown * 0.8
                if (lengthGood <= lengthSpeedDown):
                    iReturn = 0.1
            return iReturn
            
        if (currAngle > 0):
            iReturn = 0.5
            speedOK = self.calcCornerSpeed (currIdxPiece)
            speedNext = self.calcCornerSpeed (currIdxPiece + 1)
            cornerAngle = abs(self.get_AngleInCorner(currIdxPiece))
            if (cornerAngle <= 30):
                speedOK = speedOK + speedOK*(1 - float(cornerAngle)/float(30)) 
            if self.incAI > -1:
                self.incAI = self.incAI + 1;
            
            if (self.currAngle >= (self.cornerGoodAngle * 0.3)) and (self.currSpeed >= speedOK):
                self.incAI = -1
                self.set_incAI(currIdxPiece)
                
            if ((nextAngle == 0) or (abs(speedOK - speedNext) < 0.1)) and (self.incAI > 0) and (self.currSpeed >= speedOK):
                self.incAI = 0.03
                self.set_incAI(currIdxPiece)
                self.incAI = -1
                
            if (speedOK > 0) and (speedOK == speedNext):
                if self.currSpeed > speedOK:
                    iReturn = 0.05
                elif self.currSpeed > 0:
                    reduce = float(self.currSpeed - speedOK) / float(speedOK)
                    iReturn = 0.5 - reduce
                reduce2 = 0
                if lastAngle > 0:
                    reduce2 = float(self.currAngle - self.cornerGoodAngle) / float(self.cornerGoodAngle)
                if iReturn > 0:
                    iReturn =  iReturn - float(iReturn*reduce2)
                
            elif (speedNext > 0) and (self.currSpeed > speedNext):
                print("Next angle is smaller")
                if self.currSpeed > speedNext:
                    reduce = float(self.currSpeed - speedNext) / float(speedNext)
                    iReturn = 0.5 - reduce
                reduce2 = 0
                if lastAngle > 0:
                    reduce2 = float(self.currAngle - self.cornerGoodAngle) / float(self.cornerGoodAngle)
                if iReturn > 0:
                    iReturn =  iReturn - float(iReturn*reduce2)
                    
            elif (speedOK > 0):
                if self.currSpeed > speedOK:
                    iReturn = 0.05
                elif self.currSpeed > 0:
                    reduce = float(self.currSpeed - speedOK) / float(speedOK)
                    iReturn = 0.5 - reduce
                reduce2 = 0
                if lastAngle > 0:
                    reduce2 = float(self.currAngle - self.cornerGoodAngle) / float(self.cornerGoodAngle)
                if iReturn > 0:
                    iReturn =  iReturn - float(iReturn*reduce2)
                                        
                if (nextAngle == 0):
                    flatInFront = self.get_countFlatInFront(currIdxPiece + 1)
                    speedOK = self.calcCornerSpeed (currIdxPiece + flatInFront + 1)
                    if speedOK > 0:
                        lengthGood = self.get_lengthFlatInFront(currIdxPiece + 1, self.carPositions) 
                        piece = self.get_piece(currIdxPiece + 1)
                        if inPieceDistance > 0:
                            lengthGood = lengthGood + inPieceDistance
                        lengthSpeedDown = (self.currSpeed - speedOK) * 47
                        lengthSpeedDown = lengthSpeedDown #- lengthSpeedDown * 0.05
                        if (lengthGood <= lengthSpeedDown):
                            iReturn = 0
                            print(">>>>>>>Length GOOD/current SPEED/IndexPiece in last piece in corner:")
                            print lengthGood
                            print self.currSpeed
                            print currIdxPiece
                            return iReturn
                        elif (float(inPieceDistance)/float(self.get_lengthPiece(currIdxPiece)) >= 0.96) and  (float(self.currAngle) / float(self.cornerGoodAngle) < 0.86):
                            iReturn = 1
                            print(">>>>>>>Speedup before exit corner<<<")
                
            if (iReturn < 0):
                    iReturn = 0
            elif (iReturn >1):
                    iReturn = 1
            return iReturn
        
        #Defaul base on car Angle
        iReturn = 1 - float(self.currAngle) / float(self.cornerGoodAngle)
        if (iReturn < 0):
                    iReturn = 0
        elif (iReturn >1):
                    iReturn = 1
        return iReturn
        
    def get_AngleInFront(self, currIdxPiece, far):
        totalAngle = 0        
        while far != 0:            
            idx = currIdxPiece + far
            if idx >= self.get_countPiece():
                idx = currIdxPiece + far - self.get_countPiece()
            if idx < 0:
                idx = self.get_countPiece() - idx
            nextPiece = self.get_piece(idx)
            if (nextPiece is not 'null') and ('angle' in nextPiece):
                totalAngle = totalAngle + nextPiece['angle']
            if far > 0:
                far = far - 1
            elif far < 0:
                far = far + 1
        return totalAngle
    
    def get_countFlatInFront(self, currIdxPiece):
        total = 0
        while True:
            currIdxPiece = currIdxPiece + 1
            if currIdxPiece >= self.get_countPiece():
                currIdxPiece = currIdxPiece - self.get_countPiece()
            nextPiece = self.get_piece(currIdxPiece)
            #Has a corner here
            if ('angle' in nextPiece):
                if abs(self.get_AngleInCorner(currIdxPiece)) > 25:
                    break
            total = total + 1
        return total
    
    def get_lengthPiece(self, currIdxPiece):
        total = 0
        nextPiece = self.get_piece(currIdxPiece)
        #Has a corner here
        if ('angle' in nextPiece):
            total = total + (3.14 * nextPiece['radius'] *  nextPiece['angle'])/float(180)
        else:
            total = total + nextPiece['length']
        return total
    
    def get_lengthInFront(self, currIdxPiece, Count):
        total = 0
        while Count > 0:
            currIdxPiece = currIdxPiece + 1
            Count = Count - 1
            if currIdxPiece >= self.get_countPiece():
                currIdxPiece = currIdxPiece - self.get_countPiece()
            nextPiece = self.get_piece(currIdxPiece)
            #Has a corner here
            if ('angle' in nextPiece):
                total = total + (3.14 * nextPiece['radius'] *  nextPiece['angle'])/float(180)
            else:
                total = total + nextPiece['length']
        return total
    
    def get_lengthFlatInFront(self, currIdxPiece, data):
        total = 0
        while True:
            currIdxPiece = currIdxPiece + 1
            if currIdxPiece >= self.get_countPiece():
                currIdxPiece = currIdxPiece - self.get_countPiece()
            nextPiece = self.get_piece(currIdxPiece)
            #Has a corner here
            if ('angle' in nextPiece):
                break
            else:
                total = total + nextPiece['length']            
        return total
    
    def get_AngleInCorner(self, currIdxPiece):
        totalAngle = 0        
        lastAngle = 0
        lastRadius = 0
        nextPiece = self.get_piece(currIdxPiece)
        if ('angle' in nextPiece):
            lastAngle = nextPiece['angle']
        if ('radius' in nextPiece):
            lastRadius = nextPiece['radius']
            
        idx = currIdxPiece
        while (nextPiece is not 'null') and ('angle' in nextPiece):
                curAngle = nextPiece['angle']
                if (lastRadius != nextPiece['radius']):
                    break
                if lastAngle > 0:
                    if (lastAngle - curAngle) > (lastAngle + curAngle):
                        break;
                elif lastAngle < 0:
                    if (lastAngle - curAngle) < (lastAngle + curAngle):
                        break;
                lastAngle = nextPiece['angle']
                totalAngle = totalAngle + (nextPiece['angle'])
                idx = idx + 1
                if idx >= self.get_countPiece():
                    idx = 0
                nextPiece = self.get_piece(idx)
                
        idx = currIdxPiece - 1
        if idx < 0:
            idx = self.get_countPiece() - 1
        nextPiece = self.get_piece(idx)
        while (nextPiece is not 'null') and ('angle' in nextPiece):
                curAngle = nextPiece['angle']
                if (lastRadius != nextPiece['radius']):
                    break
                if lastAngle > 0:
                    if (lastAngle - curAngle) > (lastAngle + curAngle):
                        break;
                elif lastAngle < 0:
                    if (lastAngle - curAngle) < (lastAngle + curAngle):
                        break;
                lastAngle = nextPiece['angle']
                totalAngle = totalAngle + (nextPiece['angle'])
                idx = idx - 1
                if idx < 0:
                    idx = self.get_countPiece() - 1
                nextPiece = self.get_piece(idx)
                
        return totalAngle
        
    def get_radInCorner(self, currIdxPiece):
        raInCorner = 0
        piece = self.get_piece(currIdxPiece)
        if (piece is not 'null') and ('radius' in piece):
                raInCorner = piece['radius']
        return raInCorner
        
    def calcCornerSpeed(self, currIdxPiece):
        r = self.get_radInCorner(currIdxPiece)
        angle = abs(self.get_AngleInCorner(currIdxPiece))
        speedOK = 0
        if (r > 0) or (angle > 0):
            #speedOK = (2*r*3.14159265)/angle
            laneDistance = 0
            myLane = self.get_laneInPiece()
            for item in self.lanes:
                if myLane == item['index']:
                    laneDistance = item['distanceFromCenter']
                    break
            speedOK = math.sqrt(r + laneDistance)
            
            # Decrease speed
            if (r <= 40):
                speedOK = (speedOK - speedOK * 0.18)
            elif (r <= 60):
                speedOK = (speedOK - speedOK * 0.13)
            elif (r <= 70):
                speedOK = (speedOK - speedOK * 0.1)
            elif (r <= 80):
                speedOK = (speedOK - speedOK * 0.09)
            elif (r <= 90):
                speedOK = (speedOK - speedOK * 0.06)
            elif (r >= 200):
                speedOK = (speedOK + speedOK * 0.09)
            elif (r >= 180):
                speedOK = (speedOK + speedOK * 0.06)
            elif (r >= 160):
                speedOK = (speedOK + speedOK * 0.03)
                
            speedOK = speedOK  - (speedOK * self.cornerReduce)
            # AI reduce base on crash
            reduce = 0
            piece = self.get_piece(currIdxPiece)
            if (piece is not 'null') and ('reduceAI' in piece):
                    reduce = piece['reduceAI']
                    speedOK = speedOK - speedOK * reduce
                    print(">>>Reduce base crash")
                    print reduce
            # AI inc base on last corner
            if (piece is not 'null') and ('inceaseAI' in piece) and (reduce == 0):
                inc = piece['inceaseAI']
                if inc > 0:
                    speedOK = speedOK + speedOK * inc
                    print(">>>inceaseAI base last corner")
                    print inc
            
        return  speedOK
    
    def get_leftRightLane(self, myLane, nextAngle):
        myDistanceFromCenter = -1000000
        minDistanceFromCenter = 1000000
        maxDistanceFromCenter = -1000000
        for item in self.lanes:
            if myLane == item['index']:
                myDistanceFromCenter = item['distanceFromCenter']
            if minDistanceFromCenter >  item['distanceFromCenter']:
                minDistanceFromCenter = item['distanceFromCenter']
            if maxDistanceFromCenter <  item['distanceFromCenter']:
                maxDistanceFromCenter = item['distanceFromCenter']
        if (nextAngle == 0):
            if (myDistanceFromCenter < maxDistanceFromCenter):
                return "Right"
            elif (myDistanceFromCenter > minDistanceFromCenter):
                return "Left"
        elif (nextAngle > 0):
            if (myDistanceFromCenter < maxDistanceFromCenter):
                return "Right"
        elif (nextAngle < 0):
            if (myDistanceFromCenter > minDistanceFromCenter):
                return "Left"
        return "null"
                
    def get_right_lane(self, currIdxPiece, data):
        currPiece = self.get_piece(currIdxPiece)
        if 'switch' in currPiece:
            if self.lastSwitchPiece != currIdxPiece:
                flatInFront = self.get_countFlatInFront(currIdxPiece)
                nextAngle = (self.get_AngleInCorner(currIdxPiece + flatInFront + 1))
                myLane = -1000
                myDistance = 0
                frontLane = -1000
                myPieceIndex = -1
                #Get my info
                for item in data:
                    if item['id']['name'] == self.name:
                        myLane = item['piecePosition']['lane']['startLaneIndex']
                        myDistance = item['piecePosition']['inPieceDistance']
                        myPieceIndex = item['piecePosition']['pieceIndex']
                        break;
                #Get car in front
                for item in data:
                    if item['id']['name'] != self.name:
                        guestDistance = item['piecePosition']['inPieceDistance']
                        guestIndexPiece = item['piecePosition']['pieceIndex']
                        guestLane = item['piecePosition']['lane']['startLaneIndex']
                        if myPieceIndex == guestIndexPiece:
                            if myDistance < guestDistance:
                                frontLane = guestLane
                        else:
                            if (abs(myPieceIndex - guestIndexPiece) < 5):
                                if myPieceIndex < guestIndexPiece:
                                    frontLane = guestLane
                            else:
                                if myPieceIndex > guestIndexPiece:
                                    frontLane = guestLane
                                
                if (myLane > -1000) and (myLane == frontLane):
                    lane = self.get_leftRightLane(myLane, 0)
                    if lane is not 'null':
                        self.switchlane(lane)
                        self.lastSwitchPiece = currIdxPiece
                        return True                    
                
                if (myLane > -1000) and (nextAngle != 0):
                    lane = self.get_leftRightLane(myLane, nextAngle)
                    if lane is not 'null':
                        self.switchlane(lane)
                        self.lastSwitchPiece = currIdxPiece
                        return True                
        else:
            self.lastSwitchPiece = -1
        return False
        
    def tryTurbo(self, data):
        if self.turboAvailable and (self.get_AngleInCorner(self.currIdxPiece) == 0):
            self.throttleTurbo = 1
            countFlat = self.get_countFlatInFront(self.currIdxPiece - 1)
            lengthGood = self.get_lengthFlatInFront(self.currIdxPiece, self.carPositions) 
            # Speedup after car restore
            if (self.currIdxPiece == self.afterCrash) and (lengthGood > 250):
                self.turboAvailable = False
                self.msg("turbo", "I was crash. I need speedup. I love it!")
                return True
                
            if (lengthGood > 350):
                self.msg("turbo", "I love it!")
                print self.currIdxPiece
                self.turboAvailable = False
                print("JUST TURBO, I love it!")
                if countFlat < 4:                    
                    self.throttleTurbo = 0.3
                elif countFlat < 5:                    
                    self.throttleTurbo = 0.4
                elif countFlat < 6:                    
                    self.throttleTurbo = 0.5
                elif countFlat < 7:                    
                    self.throttleTurbo = 0.7
                return True
            if  self.isCarInFront(data) and (lengthGood > 100):
                self.msg("turbo", "I love it!")
                self.turboAvailable = False
                print("JUST TURBO FOR KICK CAR IN FRONT, I love it!")
                return True
            
            # For last pieces
            laps = -1
            if 'laps' in self.raceSession:
                laps = self.raceSession['laps']
            finish =  (self.get_countFlatInFront(self.currIdxPiece - 1)  + self.currIdxPiece + 1) >= self.get_countPiece()
            if (laps == self.lapCompleted + 1) and finish:
                self.msg("turbo", "I love it!")
                self.turboAvailable = False
                print("Just turbo for finish, I love it!")
                return True
        return False
        
    def ping(self):
        self.msg("ping", {})

    def run(self):
        # This function for test only. Comment for real race
        #self.creatRace()
        
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.throttle(1)

    def on_car_positions(self, data):
        if self.crash == True:
            self.ping()
        else:
            currIdxPiece = self.get_currentIdxPiece(data)
            self.currIdxPiece = currIdxPiece
            self.carPositions = data
            self.currAngle = abs(self.get_angle(data))
            self.get_speed(self.get_piecePosition(data))
            setThrottle = self.get_throttle(currIdxPiece)
            if not self.get_right_lane(currIdxPiece, data):
                if not self.tryTurbo(data):
                    self.throttle(setThrottle)
            if (self.currAngle > 20):
                print("ANGLE/THROTTLE:")
                print self.currAngle
                print setThrottle

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, data):
        print("Your car:")
        print data
        self.carName = data['name']
        self.carColor = data['color']
        print self.carName
        print self.carColor
        self.ping()

    def on_game_init(self, data):
        print("GAME INIT:")
        print('--------------------------------')
        #self.track = data['race']['track']
        self.pieces = data['race']['track']['pieces']
        self.lanes = data['race']['track']['lanes']
        self.cars = data['race']['cars']
        self.raceSession = data['race']['raceSession']
        self.lastSwitchPiece = -1
        self.lastSwitch = 'null'
        self.turboAvailable = False
        self.gameTick = -1
        self.inTurboMode = False
        self.lastPiece = 'null'
        self.currSpeed = 0
        self.crash = False
        self.lapCompleted = 0
        self.carPositions = 'null'
        print("Cars:")
        print self.cars
        print("raceSession:")
        print self.raceSession
        print("Pieces:")
        print self.pieces
        print("Lanes:")
        print self.lanes
        print('--------------------------------')
        self.ping()

    def on_game_end(self, data):
        print("Game End!")
        self.results = data['results']
        self.bestLaps = data['bestLaps']
        print self.results
        print self.bestLaps
        self.ping()
    
    def on_car_crash(self, data):
        if self.name == data['name']:
            print("Car was crashed currSpeed/lastSpeed/Piece/OKspeed")
            print self.currSpeed
            print self.lastSpeed
            print self.currIdxPiece
            print self.calcCornerSpeed (self.currIdxPiece)
            self.crash = True
            self.currAngle = 0
            self.afterCrash = -1
            self.lastPiece = 'null'
            self.currSpeed = 0
            # reduce for next time
            self.set_reduceAI(self.currIdxPiece)
        self.ping()
        #raise Exception("CRASH!!!!!!")
     
    def on_car_spawn(self, data):
        print("Car RESTORE!")
        if self.crash:
            if self.name == data['name']:
                if  self.turboAvailable and (self.get_countFlatInFront(self.currIdxPiece) > 2):
                    self.msg("turbo", "Come on baby!")
                self.afterCrash = self.currIdxPiece
                self.crash = False
        else:
            self.ping()
    
    def on_lapFinished(self, data):
        if data['car']['name'] == self.name:
            print("Lap finished!")
            self.lapFinished = data
            print data
            self.lapCompleted = self.lapCompleted + 1
            print("Number laps completed:")
            print self.lapCompleted
        self.ping()
     
    def on_tournamentEnd(self, data):
        print("Tournament End")
        self.ping()
        
    def on_dnf(self, data):
        print("Car disqualified")
        print data
        self.ping()
        
    def on_finish(self, data):
        if data['name'] == self.name:
            print("I finished this game!")
            print data
        self.ping()
    def on_turboAvailable(self, data):
        self.turboAvailable = True;
        self.turboDurationTicks = data['turboDurationTicks']
        print("Turbo available")
        print data
    
    def on_turboStart(self, data):
        if self.name == data['name']:
            self.inTurboMode = True
            print("Turbo START!!!")
            print data
            
    def on_turboEnd(self, data):
        if self.name == data['name']:
            self.inTurboMode = False
            print("Turbo END!!!")
            print data
            
    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'gameEnd': self.on_game_end,
            'tournamentEnd': self.on_tournamentEnd,
            'crash': self.on_car_crash,
            'spawn': self.on_car_spawn,
            'lapFinished': self.on_lapFinished,
            'dnf': self.on_dnf,
            'finish': self.on_finish,
            'turboAvailable': self.on_turboAvailable,
            'turboStart': self.on_turboStart,
            'turboEnd': self.on_turboEnd,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.lastGameTick = self.gameTick
                self.gameTick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
